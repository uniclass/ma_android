package com.zagulakamil.medicalassistant.fallAlarm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.zagulakamil.medicalassistant.R
import com.zagulakamil.medicalassistant.base.BaseFragment
import kotlinx.android.synthetic.main.fall_fragment.*


class FallFragment: BaseFragment(), FallContract.View {

    lateinit var presenter: FallContract.Presenter

    companion object Factory {
        fun newInstance(): FallFragment {
            return FallFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fall_fragment, container, false)
        presenter.setView(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cancelBtn.setOnClickListener {
            presenter.cancelAlarm()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.stop()
    }

    override fun finishTask() {
        Toast.makeText(activity, R.string.alarm_canceled, Toast.LENGTH_LONG).show()
        activity?.finish()
    }
}