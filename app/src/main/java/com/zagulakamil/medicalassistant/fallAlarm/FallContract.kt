package com.zagulakamil.medicalassistant.fallAlarm

import com.zagulakamil.medicalassistant.BasePresenter
import com.zagulakamil.medicalassistant.BaseView

interface FallContract {
    interface View: BaseView<Presenter> {
        fun finishTask()
    }

    interface Presenter: BasePresenter {
        fun setView(view: FallContract.View)
        fun cancelAlarm()
    }
}