package com.zagulakamil.medicalassistant.fallAlarm

import android.os.Bundle
import com.zagulakamil.medicalassistant.App
import com.zagulakamil.medicalassistant.R
import com.zagulakamil.medicalassistant.base.BaseActivity
import javax.inject.Inject

class FallActivity: BaseActivity() {
    @Inject
    lateinit var presenter: FallPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)
        App.graph.inject(this)

        val fragment = FallFragment.newInstance()
        fragment.presenter = presenter
        supportFragmentManager.beginTransaction()
                .replace(R.id.contentPanel, fragment)
                .commitAllowingStateLoss()
    }
}
