package com.zagulakamil.medicalassistant.fallAlarm

import com.zagulakamil.medicalassistant.data.local.Patient
import com.zagulakamil.medicalassistant.data.remote.RestAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import rx.Observable
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class FallPresenter: FallContract.Presenter {

    private lateinit var view: FallContract.View
    private var subscriptions = CompositeSubscription()
    private val restApi: RestAPI

    @Inject constructor(restApi: RestAPI) {
        this.restApi = restApi
    }


    override fun start() {
        subscriptions.add(Observable.timer(5, TimeUnit.SECONDS).subscribe(this::sendAlarm))
    }

    override fun stop() {}

    override fun setView(view: FallContract.View) {
        this.view = view
    }

    private fun sendAlarm(a: Long) {
        restApi.helpRequestAuto()?.observeOn(AndroidSchedulers.mainThread())?.
                subscribeOn(Schedulers.newThread())?.subscribe({ Timber.d("Request sent")
            Patient.lastAlarm = it.alarmId
        },
                { Timber.d(it, "Error while requesting error")})
    }

    override fun cancelAlarm() {
        subscriptions.clear()
        restApi.cancelAlarm(Patient.lastAlarm ?: -1)?.subscribeOn(Schedulers.newThread())?.
                observeOn(AndroidSchedulers.mainThread())?.subscribe({
            view.finishTask()
        }, { Timber.d(it, "Error while canceling")})
    }
}