package com.zagulakamil.medicalassistant

interface BasePresenter {
    fun start()
    fun stop()
}