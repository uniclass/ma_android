package com.zagulakamil.medicalassistant

import android.app.Application
import com.marcinmoskala.kotlinpreferences.PreferenceHolder
import com.zagulakamil.medicalassistant.injection.AndroidModule
import com.zagulakamil.medicalassistant.injection.ApplicationComponent
import com.zagulakamil.medicalassistant.injection.DaggerApplicationComponent
import timber.log.Timber

class App : Application() {

    companion object {
        // platformStatic allow access it from java code
        @JvmStatic lateinit var graph: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        PreferenceHolder.setContext(this)
        graph = DaggerApplicationComponent.builder().androidModule(AndroidModule(this)).build()
    }
}