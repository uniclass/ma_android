package com.zagulakamil.medicalassistant.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.zxing.integration.android.IntentIntegrator
import com.zagulakamil.medicalassistant.R
import com.zagulakamil.medicalassistant.base.BaseFragment
import com.zagulakamil.medicalassistant.data.local.Patient
import com.zagulakamil.medicalassistant.data.local.PatientModel
import com.zagulakamil.medicalassistant.main.MainActivity
import kotlinx.android.synthetic.main.login_fragment.*
import timber.log.Timber

class LoginFragment: BaseFragment(), LoginContract.View {

    lateinit var presenter: LoginContract.Presenter

    companion object Factory {
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.login_fragment, container, false)
        presenter.setView(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.start()
        launchScanner.setOnClickListener {
            val qrScanner = IntentIntegrator(activity)
            qrScanner.setOrientationLocked(true)
            qrScanner.initiateScan()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.stop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        val parsed = result?.contents ?: "null"
        if (parsed != "null") {
            try {
                val gson = Gson()
                val patient = gson.fromJson<PatientModel>(parsed)
                Patient.api = patient.server
                Patient.websocket = patient.websocket
                Patient.patientId = patient.patientId
                (activity as LoginActivity).showMainScreen()
            } catch (e: Exception) {
                presenter.wrongQrCode()
            }
        }
    }

    override fun showWrongQrCodeInfo() {
        info?.setText(R.string.login_wrong_qrcode)
    }
}