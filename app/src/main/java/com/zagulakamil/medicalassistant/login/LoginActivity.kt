package com.zagulakamil.medicalassistant.login

import android.content.Intent
import android.os.Bundle
import com.zagulakamil.medicalassistant.App
import com.zagulakamil.medicalassistant.R
import com.zagulakamil.medicalassistant.base.BaseActivity
import com.zagulakamil.medicalassistant.base.BaseFragment
import com.zagulakamil.medicalassistant.data.local.Patient
import com.zagulakamil.medicalassistant.main.MainActivity
import javax.inject.Inject


class LoginActivity: BaseActivity() {

    @Inject lateinit var presenter: LoginPresenter
    var fragment: BaseFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)
        App.graph.inject(this)

        if (Patient.patientId == null) {
            var fragment = LoginFragment.newInstance()
            fragment.presenter = presenter
            supportFragmentManager.beginTransaction()
                    .replace(R.id.contentPanel, fragment)
                    .commitAllowingStateLoss()
            this.fragment = fragment
        } else {
            showMainScreen()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }

    fun showMainScreen() {
        var intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
}