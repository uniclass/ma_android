package com.zagulakamil.medicalassistant.login

import com.zagulakamil.medicalassistant.BasePresenter
import com.zagulakamil.medicalassistant.BaseView

interface LoginContract {

    interface View: BaseView<Presenter> {
        fun showWrongQrCodeInfo()
    }

    interface Presenter: BasePresenter {
        fun setView(view: View)
        fun wrongQrCode()
    }

}
