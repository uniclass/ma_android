package com.zagulakamil.medicalassistant.login

import javax.inject.Inject

class LoginPresenter: LoginContract.Presenter {

    lateinit private var view: LoginContract.View

    @Inject constructor()

    override fun start() {
    }

    override fun stop() {
    }

    override fun setView(view: LoginContract.View) {
        this.view = view
    }

    override fun wrongQrCode() {
        view.showWrongQrCodeInfo()
    }

}