package com.zagulakamil.medicalassistant.util

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import javax.inject.Inject

class DeviceManager {
    val BLUETOOTH_REQUEST_CODE = 1212
    val LOCATION_REQUEST_CODE = 1213

    private val bluetoothAdapter: BluetoothAdapter

    @Inject constructor(bluetoothAdapter: BluetoothAdapter) {
        this.bluetoothAdapter = bluetoothAdapter
    }

    /**
     * Return true is enabled, false otherwise
     */
    fun isBluetoothEnabled(): Boolean {
        return bluetoothAdapter.isEnabled
    }

    /**
     * Asking user to enable bluetooth with #BLUETOOTH_REQUEST_CODE
     */
    fun enableBluetooth(activity: Activity) {
        val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        activity.startActivityForResult(intent, BLUETOOTH_REQUEST_CODE)
    }

    /**
     * Checks if location is enabled
     */
    fun isLocationEnabled(context: Context): Boolean {
        return ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED
    }

    fun enableLocation(activity: Activity) {
        ActivityCompat.requestPermissions(activity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
    }
}
