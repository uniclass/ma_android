package com.zagulakamil.medicalassistant


class UseCaseHandler(private val useCaseScheduler: UseCaseScheduler) {

    fun <T: UseCase.RequestValues, R: UseCase.ResponseValue> execute(
            useCase: UseCase<T, R>, values: T, callback: UseCase.UseCaseCallback<R>) {

        useCase.requestValues = values
        useCase.useCaseCallback = UiCallbackWrapper(callback, this)

        useCaseScheduler.execute(Runnable {
            useCase.run()
        })
    }

    fun <V: UseCase.ResponseValue> notifyResponse(response: V, useCaseCallback: UseCase.UseCaseCallback<V>) {
        useCaseScheduler.notifyResponse(response, useCaseCallback)
    }

    fun <V: UseCase.ResponseValue> notifyError(useCaseCallback: UseCase.UseCaseCallback<V>) {
        useCaseScheduler.onError(useCaseCallback)
    }

    private class UiCallbackWrapper<V: UseCase.ResponseValue>(
            private val callback: UseCase.UseCaseCallback<V>,
            private val useCaseHandler: UseCaseHandler) : UseCase.UseCaseCallback<V> {

        override fun onSuccess(response: V) {
            useCaseHandler.notifyResponse(response, callback)
        }

        override fun onError() {
            useCaseHandler.notifyError(callback)
        }

    }

    companion object Factory {
        fun create(): UseCaseHandler {
            return UseCaseHandler(UseCaseThreadPoolScheduler())
        }
    }
}