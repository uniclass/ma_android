package com.zagulakamil.medicalassistant.pairing

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.polidea.rxandroidble.RxBleDevice
import com.zagulakamil.medicalassistant.R
import com.zagulakamil.medicalassistant.base.BaseFragment
import kotlinx.android.synthetic.main.pairing_fragment.*

class PairingFragment: BaseFragment(), PairingContract.View {
    lateinit var presenter: PairingPresenter

    private var devices = mutableListOf<RxBleDevice>()
    companion object Factory {

        fun newInstance(): PairingFragment {
            return PairingFragment()
        }
    }
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.pairing_fragment, container, false)
        presenter.setView(this)
        presenter.start()
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        devicesList.adapter = DevicesAdapterList(context, devices)
        devicesList.setOnItemClickListener { parent, view, position, id ->
            presenter.pairWith(devices[position])
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.stop()
    }

    override fun showDevice(device: RxBleDevice) {
        devices.add(device)
        (devicesList.adapter as DevicesAdapterList).notifyDataSetChanged()
    }

    override fun hideEmptyState() {
        infoText.text = getString(R.string.pairing_choose_device)
    }

    override fun showDeviceIsPairedInfo() {
        devicesList.visibility = View.GONE
        infoText.text = getString(R.string.pairing_device_paired)
    }

    override fun closeScreen() {
        activity.finish()
    }
}
