package com.zagulakamil.medicalassistant.pairing

import com.polidea.rxandroidble.RxBleClient
import com.polidea.rxandroidble.RxBleDevice
import com.zagulakamil.medicalassistant.data.local.PolarH7HeartRateConsts
import com.zagulakamil.medicalassistant.data.local.PolarH7PairStatus
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PairingPresenter: PairingContract.Presenter {

    private lateinit var view: PairingContract.View
    private var bleClient: RxBleClient
    private var subscriptions: CompositeSubscription = CompositeSubscription()
    private var disposables = CompositeDisposable()

    @Inject constructor(bleClient: RxBleClient) {
        this.bleClient = bleClient
    }

    override fun start() {
        subscriptions.add(bleClient.scanBleDevices()
                .filter { it.bleDevice != null }
                .filter { it.bleDevice.name != null }
                .filter { it.bleDevice.name.startsWith(PolarH7HeartRateConsts.deviceName) }
                .distinct { it.bleDevice.macAddress }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    view.hideEmptyState()
                    view.showDevice(it.bleDevice)
                })
    }

    override fun stop() {
        subscriptions.clear()
        disposables.clear()
    }

    override fun setView(view: PairingContract.View) {
        this.view = view
    }

    override fun pairWith(rxBleDevice: RxBleDevice) {
        PolarH7PairStatus.isPaired = true
        PolarH7PairStatus.macAddress = rxBleDevice.macAddress
        PolarH7PairStatus.name = rxBleDevice.name

        view.showDeviceIsPairedInfo()
        disposables.add(Observable.timer(3, TimeUnit.SECONDS).subscribe { view.closeScreen() })
    }
}