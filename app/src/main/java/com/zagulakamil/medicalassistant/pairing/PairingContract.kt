package com.zagulakamil.medicalassistant.pairing

import com.polidea.rxandroidble.RxBleDevice
import com.zagulakamil.medicalassistant.BasePresenter
import com.zagulakamil.medicalassistant.BaseView

interface PairingContract {

    interface View: BaseView<Presenter> {
        fun showDevice(device: RxBleDevice)
        fun hideEmptyState()
        fun showDeviceIsPairedInfo()
        fun closeScreen()
    }

    interface Presenter: BasePresenter {
        fun setView(view: PairingContract.View)
        fun pairWith(rxBleDevice: RxBleDevice)
    }
}