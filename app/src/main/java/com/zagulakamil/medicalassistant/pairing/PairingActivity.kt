package com.zagulakamil.medicalassistant.pairing

import android.os.Bundle
import com.zagulakamil.medicalassistant.App
import com.zagulakamil.medicalassistant.R
import com.zagulakamil.medicalassistant.base.BaseActivity
import javax.inject.Inject

class PairingActivity: BaseActivity() {

    @Inject lateinit var presenter: PairingPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)
        App.graph.inject(this)
        var fragment = PairingFragment.newInstance()
        fragment.presenter = presenter
        supportFragmentManager.beginTransaction()
                .replace(R.id.contentPanel, fragment)
                .commitAllowingStateLoss()
    }
}
