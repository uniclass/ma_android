package com.zagulakamil.medicalassistant

import android.os.Handler
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * Executes asynchronous tasks using a [ThreadPoolExecutor].
 *
 *
 * See also [Executors] for a list of factory methods to newInstance common
 * [java.util.concurrent.ExecutorService]s for different scenarios.
 */
class UseCaseThreadPoolScheduler : UseCaseScheduler {

    private val handler: Handler = Handler()
    val POOL_SIZE: Int = 2
    val MAX_POOL_SIZE: Int = 4
    val TIMEOUT: Long = 30

    internal val threadPoolExecutor: ThreadPoolExecutor

    constructor() {
        threadPoolExecutor = ThreadPoolExecutor(POOL_SIZE, MAX_POOL_SIZE, TIMEOUT, TimeUnit.SECONDS,
                ArrayBlockingQueue<Runnable>(POOL_SIZE))
    }

    override fun execute(runnable: Runnable) {
        threadPoolExecutor.execute(runnable)
    }

    override fun <V : UseCase.ResponseValue> notifyResponse(response: V, useCaseCallback: UseCase.UseCaseCallback<V>) {
        handler.post {
            useCaseCallback.onSuccess(response)
        }
    }

    override fun <V : UseCase.ResponseValue> onError(useCaseCallback: UseCase.UseCaseCallback<V>) {
        handler.post {
            useCaseCallback.onError()
        }
    }
}
