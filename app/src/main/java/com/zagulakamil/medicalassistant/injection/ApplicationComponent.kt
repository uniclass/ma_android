package com.zagulakamil.medicalassistant.injection

import com.zagulakamil.medicalassistant.App
import com.zagulakamil.medicalassistant.fallAlarm.FallActivity
import com.zagulakamil.medicalassistant.login.LoginActivity
import com.zagulakamil.medicalassistant.main.MainActivity
import com.zagulakamil.medicalassistant.pairing.PairingActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidModule::class))
interface ApplicationComponent {
    fun inject(application: App)
    fun inject(application: MainActivity)
    fun inject(pairingActivity: PairingActivity)
    fun inject(loginActivity: LoginActivity)
    fun inject(fallActivity: FallActivity)
}