package com.zagulakamil.medicalassistant.injection

import android.bluetooth.BluetoothAdapter
import android.content.Context
import com.polidea.rxandroidble.RxBleClient
import com.zagulakamil.medicalassistant.App
import com.zagulakamil.medicalassistant.data.remote.RestAPI
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AndroidModule(private val application: App) {

    @Provides
    @Singleton
    @ForApplication
    fun provideApplicationContext(): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideRxBleClient(@ForApplication context: Context): RxBleClient {
        return RxBleClient.create(context)
    }

    @Provides
    fun provideBluetoothAdapter(): BluetoothAdapter {
        return BluetoothAdapter.getDefaultAdapter()
    }

    @Provides
    fun provideRestAPI(): RestAPI {
        val api = RestAPI()
        api.initialize()
        return api
    }
}