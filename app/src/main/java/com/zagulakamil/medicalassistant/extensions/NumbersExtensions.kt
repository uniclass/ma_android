package com.zagulakamil.medicalassistant.extensions

fun Long.Companion.signum(long: Long): Long {
    if (long < 0) {
        return -1
    } else if (long > 0) {
        return 1
    } else {
        return 0
    }
}

fun Int.Companion.signum(int: Int): Int {
    if (int < 0) {
        return -1
    } else if (int > 0) {
        return 1
    } else {
        return 0
    }
}