package com.zagulakamil.medicalassistant.main

import com.zagulakamil.medicalassistant.base.BaseFragment
import com.zagulakamil.medicalassistant.util.DeviceManager

abstract class MainBaseFragment: BaseFragment(), MainContract.View {
    internal lateinit var presenter: MainPresenter
    internal lateinit var deviceManager: DeviceManager
}