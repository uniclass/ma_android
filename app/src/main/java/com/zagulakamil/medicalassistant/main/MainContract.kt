package com.zagulakamil.medicalassistant.main

import android.hardware.SensorEventListener
import com.zagulakamil.medicalassistant.BasePresenter
import com.zagulakamil.medicalassistant.BaseView

interface MainContract {

    interface View: BaseView<Presenter> {
        fun showPulse(pulse: Int)
        fun askForPermissions()
        fun closePermissionScreen()
        fun checkIfBluetoothAndLocationAreEnabled(): Boolean
        fun showPairButton()
        fun showPulseCard()
        fun showDeviceInfo(name: String?, macAddress: String?)
        fun showConnectionStatus(connected: MainPresenter.ConnectionStatus)
        fun registerAccelerometerListener(listener: SensorEventListener)
    }

    interface Presenter: BasePresenter {
        fun setView(view: MainContract.View)
        fun helpRequestClick()
        fun checkIfBluetoothAndLocationAreEnabled()
    }
}