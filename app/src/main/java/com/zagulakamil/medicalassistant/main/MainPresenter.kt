package com.zagulakamil.medicalassistant.main

import com.google.gson.Gson
import com.polidea.rxandroidble.RxBleClient
import com.polidea.rxandroidble.RxBleConnection
import com.polidea.rxandroidble.RxBleDevice
import com.polidea.rxandroidble.exceptions.BleAlreadyConnectedException
import com.zagulakamil.medicalassistant.data.local.MeasurementModel
import com.zagulakamil.medicalassistant.data.local.Patient
import com.zagulakamil.medicalassistant.data.local.PolarH7HeartRateConsts
import com.zagulakamil.medicalassistant.data.local.PolarH7PairStatus
import com.zagulakamil.medicalassistant.data.remote.RestAPI
import com.zagulakamil.medicalassistant.data.remote.WebSocketClient
import com.zagulakamil.medicalassistant.util.DeviceManager
import d
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import rx.subjects.PublishSubject
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import java.net.URI
import java.util.*
import javax.inject.Inject


class MainPresenter: MainContract.Presenter {
    lateinit private var view: MainContract.View

    var gson: Gson
    val restApi: RestAPI
    var websocket = WebSocketClient(URI(Patient.websocket))
    val bleClient: RxBleClient
    val deviceManager: DeviceManager
    var pulseDisposable: Disposable? = null

    var subscriptions = CompositeSubscription()
    var bleDevice: RxBleDevice? = null
    var disconnectTriggerSubject = PublishSubject.create<Unit>()
    val fallDetectionListener = PersonStateListener()

    @Inject constructor(bleClient: RxBleClient, deviceManager: DeviceManager, restApi: RestAPI) {
        this.bleClient = bleClient
        this.deviceManager = deviceManager
        this.gson = Gson()
        this.restApi = restApi
    }

    override fun start() {
        this.view = view

        if (view.checkIfBluetoothAndLocationAreEnabled()) {
            setupScreen()
        } else {
            showPermissionsScreen()
        }

        websocket = WebSocketClient(URI(Patient.websocket))
        val result = websocket.connectBlocking()
        d { "Connection result = $result" }
    }

    private fun showPermissionsScreen() {
        view.askForPermissions()
    }

    private fun setupScreen() {
        if (PolarH7PairStatus.isPaired) {
            view.showPulseCard()
            view.showDeviceInfo(PolarH7PairStatus.name, PolarH7PairStatus.macAddress)
            startSearchingDevice()
        } else {
            view.showPairButton()
        }
        view.registerAccelerometerListener(fallDetectionListener)
    }

    private fun startSearchingDevice() {
        subscriptions.add(
                bleClient.scanBleDevices()
                        .filter { it != null }
                        .filter { it.bleDevice != null }
                        .filter { it.bleDevice.macAddress == PolarH7PairStatus.macAddress }
                        .take(1)
                        .flatMap {
                            bleDevice = it.bleDevice
                            listenForConnectionChanges()
                            it.bleDevice.establishConnection(false)
                        }
                        .takeUntil(disconnectTriggerSubject)
                        .doOnNext {
                            view.showConnectionStatus(ConnectionStatus.CONNECTED)
                        }
                        .flatMap { it.setupNotification(PolarH7HeartRateConsts.heartRateUuid) }
                        .flatMap { notification -> notification }
                        .subscribeOn(rx.schedulers.Schedulers.io())
                        .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                        .subscribe({
                            d { "Received ${Arrays.toString(it)}" }
                            if (it[PolarH7HeartRateConsts.heartRateStatusIndex] == PolarH7HeartRateConsts.heartRateCorrectStatus) {
                                view.showConnectionStatus(ConnectionStatus.CONNECTED)
                                view.showPulse(it[PolarH7HeartRateConsts.heartRateIndex].toInt())
                                sendPulseToBackend(Math.abs(it[PolarH7HeartRateConsts.heartRateIndex].toInt()))
                            } else {
                                view.showConnectionStatus(ConnectionStatus.ERROR)
                            }
                        }, {
                            if (it !is BleAlreadyConnectedException) {
                                d(it, { "Connection error" })
                                startSearchingDevice()
                            }
                        })
        )
    }

    private fun sendPulseToBackend(pulse: Int) {
        var measurement = MeasurementModel(Patient.patientId!!, pulse, Date().toString())
        var strMeasurement = gson.toJson(measurement)
        websocket.send(strMeasurement)
    }

    fun listenForConnectionChanges() {
        subscriptions.add(bleDevice!!.observeConnectionStateChanges().subscribe { next ->
            when (next) {
                RxBleConnection.RxBleConnectionState.CONNECTED -> {
                    Timber.d("Device connected")
                    view.showConnectionStatus(ConnectionStatus.CONNECTED)
                }
                RxBleConnection.RxBleConnectionState.CONNECTING ->
                        Timber.d("Device connecting")
                RxBleConnection.RxBleConnectionState.DISCONNECTED -> {
                    Timber.d("Device disconnected")
                    view.showConnectionStatus(ConnectionStatus.DISCONNECTED)
                }
                RxBleConnection.RxBleConnectionState.DISCONNECTING ->
                        Timber.d("Device disconnecting")
            }

        })
    }

    override fun stop() {
        websocket.close()
        pulseDisposable?.dispose()
    }

    override fun helpRequestClick() {
        restApi.initialize()
        restApi.helpRequestManual()?.observeOn(AndroidSchedulers.mainThread())?.
                subscribeOn(Schedulers.newThread())?.subscribe({ Timber.d("Request sent")},
                { Timber.d(it, "Error while requesting") })
    }

    override fun setView(view: MainContract.View) {
        this.view = view
    }

    override fun checkIfBluetoothAndLocationAreEnabled() {
        if (view.checkIfBluetoothAndLocationAreEnabled()) {
            view.closePermissionScreen()
        }
    }

    enum class ConnectionStatus {
        CONNECTED,
        DISCONNECTED,
        ERROR
    }

    fun unpairDevice() {
        subscriptions.clear()
        disconnectTriggerSubject.onNext(null)
        PolarH7PairStatus.clear()
        setupScreen()
    }
}