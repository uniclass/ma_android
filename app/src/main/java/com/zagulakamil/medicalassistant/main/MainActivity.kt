package com.zagulakamil.medicalassistant.main

import android.content.Intent
import android.os.Bundle
import com.zagulakamil.medicalassistant.App
import com.zagulakamil.medicalassistant.R
import com.zagulakamil.medicalassistant.base.BaseActivity
import com.zagulakamil.medicalassistant.util.DeviceManager
import timber.log.Timber
import javax.inject.Inject

class MainActivity: BaseActivity() {

    @Inject
    lateinit var presenter: MainPresenter
    @Inject
    lateinit var deviceManager: DeviceManager

    private var fragment: MainBaseFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)
        App.graph.inject(this)
        showMainScreen()
    }

    override fun onResume() {
        super.onResume()
        if (fragment is MainBaseFragment) {
            (fragment as MainBaseFragment).presenter = presenter
            (fragment as MainBaseFragment).deviceManager = deviceManager
        }
    }

    internal fun showPermissionsScreen() {
        var fragment = PermissionFragment.newInstance()
        fragment.presenter = presenter
        fragment.deviceManager = deviceManager
        supportFragmentManager.beginTransaction()
                .replace(R.id.contentPanel, fragment)
                .commitAllowingStateLoss()
        this.fragment = fragment
    }

    fun showMainScreen() {
        startLockTask()
        var fragment = MainFragment.newInstance()
        fragment.presenter = presenter
        fragment.deviceManager = deviceManager
        supportFragmentManager.beginTransaction()
                .replace(R.id.contentPanel, fragment)
                .commitAllowingStateLoss()
        this.fragment = fragment
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }
}