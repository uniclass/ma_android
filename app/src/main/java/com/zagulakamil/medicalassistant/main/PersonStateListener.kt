package com.zagulakamil.medicalassistant.main

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import rx.subjects.BehaviorSubject
import timber.log.Timber


class PersonStateListener: SensorEventListener {

    private var prevState = State.NONE

    private val sigma = 0.5f
    private val th = 10.0f
    private val th1 = 5.0f
    private val th2 = 2.0f
    private val fallAcceleration = (2 * 9.8)

    private val BUF_SIZE = 50
    private val window = DoubleArray(BUF_SIZE)

    private val stateSubject = BehaviorSubject.create<State>()
    private val fallDetectionSubject = BehaviorSubject.create<Boolean>()

    constructor() {
        for (i in window.indices) {
            window[i] = 0.0
        }
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_ACCELEROMETER) {
            val ax = event.values[0]
            val ay = event.values[1]
            val az = event.values[2]
            normalize(ax, ay, az)
            val currentState = state_recognition(window, ay)
            stateSubject.onNext(currentState)

            val fallDetection = detectFall(window)
            fallDetectionSubject.onNext(fallDetection)
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        // empty
    }

    private fun state_recognition(win: DoubleArray, ay: Float): State {
        val zrc = computeZrc(win)
        return when(zrc) {
                0 -> {
                    if (Math.abs(ay) < th1) {
                        // Sitting
                        State.SITTING
                    } else {
                        // Standing
                        State.STANDING
                    }
                }
                else -> {
                    if (zrc < th2) {
                        // Walking
                        State.WALKING
                    } else {
                        // none
                        State.NONE
                    }
                }
            }
    }

    private fun detectFall(win: DoubleArray): Boolean {
        var min:Double = 0.0
        var max:Double = 0.0
        for (value in win) {
            if (value < min) {
                min = value
            }
            if (value > max) {
                max = value
            }
        }

        return Math.abs(min - max) > fallAcceleration
    }

    private fun computeZrc(win: DoubleArray): Int {
        var count = 0
        var i = 1
        while (i <= this.BUF_SIZE - 1) {
            if (win[i] - th < sigma && win[i - 1] - th > sigma) {
                count += 1
            }
            i++
        }
        return count
    }

    private fun normalize(ax: Float, ay: Float, az: Float) {
        val norm = Math.sqrt((ax * ax + ay * ay + az * az).toDouble())

        var i = 0
        while (i <= this.BUF_SIZE - 2) {
            window[i] = window[i + 1]
            i++
        }

        window[this.BUF_SIZE - 1] = norm
    }

    enum class State {
        SITTING, STANDING, WALKING, NONE
    }

    fun getPersonStateChangeObservable() = stateSubject.asObservable()

    fun getFallDetectionObservable() = fallDetectionSubject.asObservable()
}