package com.zagulakamil.medicalassistant.main

import android.content.Intent
import android.hardware.SensorEventListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zagulakamil.medicalassistant.R
import com.zagulakamil.medicalassistant.login.LoginActivity
import com.zagulakamil.medicalassistant.pairing.PairingActivity
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment: MainBaseFragment() {

    companion object Factory {
        fun newInstance() : MainFragment {
            return MainFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.main_fragment, container, false)
        presenter.setView(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()

        pairBtn.setOnClickListener {
            startActivity(Intent(activity, PairingActivity::class.java))
        }

        helpRequestBtn.setOnClickListener {
            presenter.helpRequestClick()
        }

        deviceInfo.setOnLongClickListener {
            presenter.unpairDevice()
            true
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.stop()
    }

    private fun setupListeners() {
        helpRequestBtn.setOnClickListener { presenter.helpRequestClick() }
    }

    override fun showPulse(pulse: Int) {
        this.pulse.text = getString(R.string.pulse, pulse)
    }

    override fun askForPermissions() {
        (activity as MainActivity).showPermissionsScreen()
    }

    override fun checkIfBluetoothAndLocationAreEnabled(): Boolean {
        return deviceManager.isBluetoothEnabled() && deviceManager.isLocationEnabled(context)
    }

    override fun closePermissionScreen() {
        // ignore this method
    }

    override fun showPairButton() {
        pairCard.visibility = View.VISIBLE
        pulseCard.visibility = View.GONE
    }

    override fun showPulseCard() {
        pairCard.visibility = View.GONE
        pulseCard.visibility = View.VISIBLE
    }

    override fun showDeviceInfo(name: String?, macAddress: String?) {
        deviceInfo.text = getString(R.string.device_info, name, macAddress)
    }

    override fun showConnectionStatus(connected: MainPresenter.ConnectionStatus) {
        activity?.runOnUiThread {
            when (connected) {
                MainPresenter.ConnectionStatus.CONNECTED -> deviceConnection.text = getString(R.string.connection_status_connected)
                MainPresenter.ConnectionStatus.DISCONNECTED -> deviceConnection.text = getString(R.string.connection_status_disconnected)
                MainPresenter.ConnectionStatus.ERROR -> deviceConnection.text = getString(R.string.connection_status_error)
            }
        }
    }

    override fun registerAccelerometerListener(listener: SensorEventListener) {
        activity?.startService(Intent(activity, AccelerometerService::class.java))
    }
}
