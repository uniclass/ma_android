package com.zagulakamil.medicalassistant.main

import android.content.Intent
import android.hardware.SensorEventListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zagulakamil.medicalassistant.R
import kotlinx.android.synthetic.main.permission_fragment.*
import timber.log.Timber

class PermissionFragment: MainBaseFragment() {

    private var refreshPermissions = false

    companion object Factory {
        fun newInstance(): PermissionFragment {
            return PermissionFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater?.inflate(R.layout.permission_fragment, container, false)
        presenter.setView(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        enableBtn.setOnClickListener {
            enableLocationOrBluetooth()
        }
    }

    private fun enableLocationOrBluetooth() {
        if (deviceManager.isLocationEnabled(context)) {
            if (!deviceManager.isBluetoothEnabled()) {
                deviceManager.enableBluetooth(activity)
            }
        } else {
            deviceManager.enableLocation(activity)
        }
        refreshPermissions = true
    }

    override fun askForPermissions() {
        throw UnsupportedOperationException("This fragment not supports askForPermissions method")
    }

    override fun showPulse(pulse: Int) {
        throw UnsupportedOperationException("This fragment not supports showPulse method")
    }

    override fun onResume() {
        super.onResume()
        if (refreshPermissions) {
            presenter.checkIfBluetoothAndLocationAreEnabled()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == deviceManager.BLUETOOTH_REQUEST_CODE) {
            presenter.checkIfBluetoothAndLocationAreEnabled()
        } else if (requestCode == deviceManager.LOCATION_REQUEST_CODE){
            presenter.checkIfBluetoothAndLocationAreEnabled()
        }
    }

    override fun closePermissionScreen() {
        (activity as MainActivity).showMainScreen()
    }

    override fun checkIfBluetoothAndLocationAreEnabled(): Boolean {
        val enabled = deviceManager.isBluetoothEnabled() && deviceManager.isLocationEnabled(context)
        if (!enabled) {
            enableLocationOrBluetooth()
        }
        return enabled
    }

    override fun showPairButton() {
        throw UnsupportedOperationException("This fragment not supports showPairButton method")
    }

    override fun showPulseCard() {
        throw UnsupportedOperationException("This fragment not supports showPulseCard method")
    }

    override fun showConnectionStatus(connected: MainPresenter.ConnectionStatus) {
        throw UnsupportedOperationException("This fragment not supports showConnectionStatus method")
    }

    override fun showDeviceInfo(name: String?, macAddress: String?) {
        throw UnsupportedOperationException("This fragment not supports showDeviceInfo method")
    }

    override fun registerAccelerometerListener(listener: SensorEventListener) {
        throw UnsupportedOperationException("This fragment not supports registerAccelerometerListener method")
    }
}
