package com.zagulakamil.medicalassistant.main

import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.IBinder
import com.zagulakamil.medicalassistant.fallAlarm.FallActivity
import rx.Observable
import rx.subjects.PublishSubject
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import java.util.concurrent.TimeUnit

class AccelerometerService: Service() {

    private lateinit var listener: PersonStateListener
    private var subscriptions: CompositeSubscription = CompositeSubscription()
    private var currentState: PersonStateListener.State? = null
    private var currentFall: Boolean = false
    private var fallSubject = PublishSubject.create<Boolean>()

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Timber.d("OnCreateAccelerometerService")
        this.listener = PersonStateListener()

        val sensorService = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val accelerometer = sensorService.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        sensorService.registerListener(this.listener, accelerometer, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("OnDestroyAccelerometerService")
        val sensorService = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorService.unregisterListener(this.listener)
        subscriptions.clear()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        subscriptions.add(listener.getPersonStateChangeObservable().distinctUntilChanged().subscribe(this::onPersonStateChanged))
        subscriptions.add(listener.getFallDetectionObservable().distinctUntilChanged().subscribe(this::onFallDetectionStatusChanged))
        subscriptions.add(fallSubject.asObservable().throttleWithTimeout(5, TimeUnit.SECONDS).subscribe {
            val intent = Intent(this, FallActivity::class.java)
            intent.flags += Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        })
        return START_STICKY
    }

    private fun onPersonStateChanged(state: PersonStateListener.State) {
        currentState = state
    }

    private fun onFallDetectionStatusChanged(fall: Boolean) {
        currentFall = fall
        Observable.timer(1000, TimeUnit.MILLISECONDS).take(1).subscribe {
            if (currentFall) {
                if (currentState == PersonStateListener.State.NONE || currentState == PersonStateListener.State.SITTING) {
                    fallSubject.onNext(true)
                    Timber.d("Fall detected")
                }
            }
        }
    }

}