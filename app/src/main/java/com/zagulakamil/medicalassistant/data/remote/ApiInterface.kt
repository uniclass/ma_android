package com.zagulakamil.medicalassistant.data.remote

import com.zagulakamil.medicalassistant.data.remote.model.HelpRequestCancel
import com.zagulakamil.medicalassistant.data.remote.model.HelpRequestModel
import com.zagulakamil.medicalassistant.data.remote.model.HelpRequestResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiInterface {
    @POST("help")
    fun helpRequest(@Body helpRequestBody: HelpRequestModel): Observable<HelpRequestResponse>

    @POST("help/cancel")
    fun cancelAlarm(@Body id: HelpRequestCancel): Observable<Unit>
}