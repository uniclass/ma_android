package com.zagulakamil.medicalassistant.data.local

import java.util.*

object PolarH7HeartRateConsts {
    val heartRateUuid = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb")
    val deviceName = "Polar H7"
    val heartRateStatusIndex = 0
    val heartRateCorrectStatus:Byte = 22
    val heartRateIndex = 1
}