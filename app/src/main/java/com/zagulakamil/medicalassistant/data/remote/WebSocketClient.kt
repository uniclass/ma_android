package com.zagulakamil.medicalassistant.data.remote

import android.util.Log
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.lang.Exception
import java.net.URI

class WebSocketClient(uri: URI): WebSocketClient(uri) {

    override fun onOpen(handshakedata: ServerHandshake?) {
        Log.d("WEBSOCKET", "onOpen, handshake: ${handshakedata}")
    }

    override fun onClose(code: Int, reason: String?, remote: Boolean) {
        Log.d("WEBSOCKET", "${code}: ${reason}, remote: ${remote}")
    }

    override fun onMessage(message: String?) {
        Log.d("WEBSOCKET", message);
    }

    override fun onError(ex: Exception?) {
        Log.d("WEBSOCKET", "Error ${ex.toString()}")
    }

}