package com.zagulakamil.medicalassistant.data.local

import com.marcinmoskala.kotlinpreferences.PreferenceHolder

object Patient: PreferenceHolder() {
    var patientId: Int? by bindToPreferenceFieldNullable()
    var api: String? by bindToPreferenceFieldNullable()
    var websocket: String? by bindToPreferenceFieldNullable()
    var lastAlarm: Int? by bindToPreferenceFieldNullable()
}

