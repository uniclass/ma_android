package com.zagulakamil.medicalassistant.data.remote.model

data class HelpRequestModel(val id: Int, val type: Int)

data class HelpRequestResponse(val success: Boolean, val alarmId: Int)

data class HelpRequestCancel(val id: Int)