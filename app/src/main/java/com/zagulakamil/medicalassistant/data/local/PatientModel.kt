package com.zagulakamil.medicalassistant.data.local

data class PatientModel(var patientId: Int, var server: String, var websocket: String)
