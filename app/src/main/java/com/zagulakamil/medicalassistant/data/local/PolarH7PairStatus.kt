package com.zagulakamil.medicalassistant.data.local

import com.marcinmoskala.kotlinpreferences.PreferenceHolder

object PolarH7PairStatus: PreferenceHolder() {
    var isPaired: Boolean by bindToPreferenceField(false)
    var macAddress: String? by bindToPreferenceFieldNullable()
    var name: String? by bindToPreferenceFieldNullable()
}