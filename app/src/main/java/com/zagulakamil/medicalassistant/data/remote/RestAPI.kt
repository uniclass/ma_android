package com.zagulakamil.medicalassistant.data.remote

import com.zagulakamil.medicalassistant.data.local.Patient
import com.zagulakamil.medicalassistant.data.remote.model.HelpRequestCancel
import com.zagulakamil.medicalassistant.data.remote.model.HelpRequestModel
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class RestAPI {
    private var api: ApiInterface? = null

    fun initialize() {
        val retrofit = Retrofit.Builder()
                .baseUrl(Patient.api)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        this.api = retrofit.create(ApiInterface::class.java)
    }

    fun helpRequestManual() = api?.helpRequest(HelpRequestModel(Patient.patientId ?: 0, 1))

    fun helpRequestAuto() = api?.helpRequest(HelpRequestModel(Patient.patientId ?: 0, 2))

    fun cancelAlarm(id: Int) = api?.cancelAlarm(HelpRequestCancel(id))
}