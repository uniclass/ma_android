package com.zagulakamil.medicalassistant.data.local

data class MeasurementModel(var patientId: Int, var pulse: Int, var date: String)